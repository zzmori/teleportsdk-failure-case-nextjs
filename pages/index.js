import styles from '../styles/Home.module.css';
import { TeleportBridge } from 'teleport-sdk'
import React from 'react';


export default function Index() {
  const [bridge, setBridge] = React.useState(null)
  const [amt, setAmt] = React.useState(0)

  React.useEffect(() => {
    setBridge(
        new TeleportBridge({
            srcDomain: 'arbitrum'
        })
    )

    bridge.getAmounts({
        withdrawn: 1e18
    }).then(
        ({mintable}) => setAmt(mintable.toNumber())
    )
  })
  return (
    <div className={styles.container}>
      <main>
        <h1 className={styles.title}>
            {amt}
        </h1>
      </main>
    </div>
  )
}